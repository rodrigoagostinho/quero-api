class Api::V1::StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_student, only: [:show]

  # find student by id
  def show
    render json: @student
  end

  def create
    @student = Student.create(students_params)

    if @student.save
      render json: @student, status: :created
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  private
  def students_params
    params.require(:student).permit(:name, :cpf, :emails, :enem_score, :user_id, :document)
  end

  def set_student
    @student = Student.find_by_user_id(params[:id])
  end
end
