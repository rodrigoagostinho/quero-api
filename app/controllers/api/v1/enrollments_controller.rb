require 'date'

class Api::V1::EnrollmentsController < ApplicationController
  before_action :authenticate_user!

  # create enrollment
  # post /enrollments
  def create
    @enrollment = Enrollment.new(enrollment_params)
    generate_error = "Error to create enrollments"

    # generate enrollments
    enrollments = generate_enrollments(@enrollment)

    # render error if not generate correct enrollments
    render json: {
      status: 500,
      message: generate_error
    } if enrollments.length != @enrollment.quantity || enrollments.length == 0

    # save enrollments
    Enrollment.transaction do
      success = enrollments.each(&:save!)
      if success
        render json: enrollments
      else
        render json: {error: 500, message: generate_error}
      end
    end

  end

  def update_by_student_id
    # set params
    student_id = params[:student_id]
    kind = params[:kind]
    date = params[:date]

    # find student
    student = Student.find(student_id)

    # render error if student doesn't exist
    render json: {error: 500, message: "Invalid student"} if student == nil

    # find student enrollments
    enrollments = Enrollment
      .where("student_id = ? AND due_date >= ?", student_id, date)

    # update enrollments
    Enrollment.transaction do
      enrollments.map do |e|
        e.payment_day = e.due_date.day
        e.kind = kind
        unless e.save!
          render json: {error: 500, message: "Error to save enrollment"}
        end
      end

      render json: enrollments
    end

  end

  # start private methods #####################################################
  private

  # this method is used only to create
  # it will increment due date if greater than current date (today)
  def enrollment_params
    current_date = Date.current
    payment_day = params[:enrollment][:payment_day]

    # create due date and increment
    due_date = Date.new(current_date.year, current_date.month, payment_day.to_i)
    due_date = due_date.to_date >> 1 if current_date.to_date > due_date.to_date
    params[:enrollment][:due_date] = due_date

    params.require(:enrollment)
      .permit(:due_date, :kind, :quantity, :student_id, :payment_day)
  end

  def generate_enrollments(data = {})
    enrollments = []
    current_due = data.due_date

    # add first on enrollments
    enrollments.push(set_enrollment(data))

    # each others
    (1..data.quantity - 1).each do |i|
      data.due_date = data.due_date >> 1
      enrollments.push(set_enrollment(data))
    end

    enrollments
  end

  def set_enrollment(enrollment = {})
    Enrollment.new(
      :due_date => enrollment.due_date,
      :kind => enrollment.kind,
      :quantity => enrollment.quantity,
      :student_id => enrollment.student_id,
      :payment_day => enrollment.payment_day
    )
  end

end
