class Student < ApplicationRecord

  # relationship
  has_many :enrollment
  belongs_to :user, optional: false

  # upload
  mount_uploader :document, DocumentUploader

  # uniq
  validates_uniqueness_of :cpf

  # fields validation
  validates :name, presence: true
  validates :cpf, presence: true
  validates :emails, presence: true
  validates :enem_score, presence: true
  validates :document, presence: true

  # field valdation by method
  validate :validate_enem_score

  # validation methods
  def validate_enem_score
    if enem_score.present? && enem_score <= 450
      errors.add(:enem_score, "Your score should be greater than 450")
    end
  end

end
