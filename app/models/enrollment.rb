require 'date'

class Enrollment < ApplicationRecord
  belongs_to :student, optional: false

  attr_accessor :payment_day

  # validations attribs
  validates :kind, presence: true
  validates :quantity, presence: true
  validates :payment_day, presence: true
  validates_numericality_of :quantity, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 12

  # validation methods
  validate :validate_kind

  def validate_kind
    if (kind.present? && kind != 'card' && kind != 'bank_slip')
      errors.add(:kind, "Invalid kind")
    end
  end

end
