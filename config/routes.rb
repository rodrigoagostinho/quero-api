Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :api do
    namespace :v1 do
      resources :students, :only => [:show, :create]

      resources :enrollments, :only => [:create] do
        collection do
          put :update_by_student_id
        end
      end

    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
