# README

* Ruby version
> 5.2.1

* Configuration

  > bundle install

* Database creation

  > rails db:create

* Database initialization

  > rails db:migrate

* How to run the test suite

  > rspec

* Using devise to create user
  > rake routes
