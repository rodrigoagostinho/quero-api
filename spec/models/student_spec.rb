require 'rails_helper'

RSpec.describe Student, type: :model do

  before(:each) do
    @user = User.new(:email => "email@email.com", :password => "12345678",
      :password_confirmation => "12345678")
    @user.save!
    @document = Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/vader.png"))
  end

  let(:student) {
    Student.new(
      :name => "Student name",
      :cpf => "123.123.123-12",
      :emails => "email@email.com",
      :enem_score => 800,
      :user_id => @user.id,
      :document => @document
    )
  }

  it 'name cannot be nil' do
    student.name = nil
    expect(student).to_not be_valid
  end

  it 'cpf cannot be nil' do
    student.cpf = nil
    expect(student).to_not be_valid
  end

  it 'emails cannot be nil' do
    student.emails = nil
    expect(student).to_not be_valid
  end

  it 'enem score cannot be nil' do
    student.enem_score = nil
    expect(student).to_not be_valid
  end

  it 'enem score cannot be less than 450' do
    student.enem_score = 340
    expect(student).to_not be_valid

    student.enem_score = 450
    expect(student).to_not be_valid

    student.enem_score = 100
    expect(student).to_not be_valid
  end

  it 'document cannot be nil' do
    student.document = nil
    expect(student).to_not be_valid
  end

  it 'document cannot be docx' do
    student.document = Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/doc.docx"))
    expect(student).to_not be_valid
  end

  it 'should works' do
    expect(student).to be_valid
  end

end
