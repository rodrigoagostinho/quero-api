require 'rails_helper'
require 'date'

RSpec.describe Enrollment, type: :model do

  before(:each) do
    @document =  Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/vader.png"))
    @user = User.new(:email => "email@email.com", :password => "12345678",
      :password_confirmation => "12345678")
    @user.save!

    @student = Student.new(
      :name => "Student name",
      :cpf => "123.123.123-12",
      :emails => "email@email.com",
      :enem_score => 800,
      :user_id => @user.id,
      :document => @document
    )
    @student.save!
  end

  let(:enrollment) { Enrollment.new(:payment_day => 10,
    :kind => 'card', :student_id => @student.id, :quantity => 10) }

  it 'kind cannot be nil' do
    enrollment.kind = nil
    expect(enrollment).to_not be_valid
  end

  it 'kind should be card or bank_slip' do
    enrollment.kind = 'test'
    expect(enrollment).to_not be_valid
  end

  it 'student_id cannot be nil' do
    enrollment.student_id = nil
    expect(enrollment).to_not be_valid
  end

  it 'quantity cannot be nil' do
    enrollment.quantity = nil
    expect(enrollment).to_not be_valid
  end

  it 'quantity cannot be 0' do
    enrollment.quantity = 0
    expect(enrollment).to_not be_valid
  end

  it 'quantity cannot be 13' do
    enrollment.quantity = 13
    expect(enrollment).to_not be_valid
  end

  it 'payment_day cannot be nil' do
    enrollment.payment_day = nil
    expect(enrollment).to_not be_valid
  end

  it 'should works' do
    expect(enrollment).to be_valid
  end

end
