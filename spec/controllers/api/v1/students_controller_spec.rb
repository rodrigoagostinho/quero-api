require 'rails_helper'

RSpec.describe Api::V1::StudentsController, type: :controller do

  before(:each) do
    @document =  Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/vader.png"))
    @user = User.new(:email => "email@email.com", :password => "12345678",
      :password_confirmation => "12345678")
    @user.save!

    @user2 = User.new(:email => "email1@email.com", :password => "12345678",
      :password_confirmation => "12345678")
    @user2.save!
  end

  let(:student) {
    Student.new(:name => "Student name", :cpf => "123.123.123-12",
      :emails => "email@email.com", :enem_score => 800, :user_id => @user.id,
      :document => @document)
  }

  let(:student2) {
    Student.new(:name => "Student name 2", :cpf => "123.123.123-14",
      :emails => "email2@email.com", :enem_score => 500, :user_id => @user2.id,
      :document => @document)
  }

  let(:user_3) {
    User.new(:email => "email@email1.com", :password => "12345678",
      :password_confirmation => "12345678")
  }

  it 'should status 200 and find student by id' do
    student.save!
    student2.save!

    auth_headers = @user.create_new_auth_token
    request.headers['Accept'] =  "application/vnd.api+json"
    request.headers['access-token'] =  auth_headers['access-token']
    request.headers['client'] =  auth_headers['client']
    request.headers['uid'] =  auth_headers['uid']

    get :show, params: {id: @user.id}

    response_body = JSON.parse(response.body)

    expect(response.status).to eq(200)
    expect(response_body.fetch('id')).to eq(student.id)
    expect(response_body.fetch('id')).to_not eq(student2.id)
  end

  it 'should create student' do
    user_3.save!

    auth_headers = user_3.create_new_auth_token
    request.headers['Accept'] =  "application/vnd.api+json"
    request.headers['access-token'] =  auth_headers['access-token']
    request.headers['client'] =  auth_headers['client']
    request.headers['uid'] =  auth_headers['uid']

    post :create, params: {student: {:name => "Student name 3", :cpf => "123.123.123-15",
      :emails => "email3@email.com", :enem_score => 500, :user_id => user_3.id, :document => @document}}

    response_body = JSON.parse(response.body)

    expect(response.status).to eq(201)
    expect(response_body.fetch('cpf')).to eq('123.123.123-15')
    expect(response_body.fetch('emails')).to eq('email3@email.com')
    expect(response_body.fetch('cpf')).to_not eq(student.cpf)
  end

end
