require 'rails_helper'
require 'date'

RSpec.describe Api::V1::EnrollmentsController, type: :controller do

  before(:each) do
    @document =  Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/vader.png"))
    @user = User.new(:email => "email@email.com", :password => "12345678",
      :password_confirmation => "12345678")
    @user.save!

    @student = Student.new(
      :name => "Student name",
      :cpf => "123.123.123-12",
      :emails => "email@email.com",
      :enem_score => 800,
      :user_id => @user.id,
      :document => @document
    )
    @student.save!

    auth_headers = @user.create_new_auth_token
    request.headers['Accept'] =  "application/vnd.api+json"
    request.headers['access-token'] =  auth_headers['access-token']
    request.headers['client'] =  auth_headers['client']
    request.headers['uid'] =  auth_headers['uid']
  end

  it 'should create enrollment with correct equal current date' do
    quantity = 2
    current_date = Date.current
    post :create, params: {enrollment: {:payment_day => current_date.day, :kind => 'card',
      :student_id => @student.id, :quantity => quantity}}

    response_body = JSON.parse(response.body)

    expect(response.status).to eq(200)
    expect(response_body.length).to eq(quantity)
    expect(response_body.length).to_not eq(quantity + 1)
    expect(response_body[0].fetch('due_date').to_date).to be == current_date.to_date
  end

  it 'should create enrollment with payment_day less than current_date' do
    quantity = 1
    current_date = Date.current

    post :create, params: {enrollment: {:payment_day => current_date.day - 1, :kind => 'card',
      :student_id => @student.id, :quantity => quantity}}

    response_body = JSON.parse(response.body)

    right_date = current_date - 1
    right_date = right_date >> 1

    expect(response.status).to eq(200)
    expect(response_body.length).to eq(quantity)
    expect(response_body.length).to_not eq(quantity + 1)
    expect(response_body[0].fetch('due_date').to_date).to be == right_date
  end

  it 'should create enrollment with payment_day greater than current_date' do
    quantity = 1
    current_date = Date.current

    post :create, params: {enrollment: {:payment_day => current_date.day + 1, :kind => 'card',
      :student_id => @student.id, :quantity => quantity}}

    response_body = JSON.parse(response.body)

    right_date = current_date + 1

    expect(response.status).to eq(200)
    expect(response_body.length).to eq(quantity)
    expect(response_body.length).to_not eq(quantity + 1)
    expect(response_body[0].fetch('due_date').to_date).to be == right_date
  end

  it 'should update next enrollments' do
    current_date = Date.current
    Enrollment.new(:payment_day => current_date.day - 1, :kind => 'card',
      :student_id => @student.id, :quantity => 2, :due_date => Date.new(2018, 12, current_date.day - 1)).save!
    Enrollment.new(:payment_day => current_date.day, :kind => 'card',
      :student_id => @student.id, :quantity => 2, :due_date => Date.new(2018, 12, current_date.day)).save!

    put :update_by_student_id, params: {:student_id => @student.id, :kind => 'bank_slip', "date" => "2017-11-11"}

    response_body = JSON.parse(response.body)

    expect(response.status).to eq(200)
    expect(response_body[0].fetch('kind')).to eq('bank_slip')
  end

end
