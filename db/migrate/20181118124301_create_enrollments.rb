class CreateEnrollments < ActiveRecord::Migration[5.2]
  def change
    create_table :enrollments do |t|
      t.date :due_date
      t.string :kind
      t.references :student, foreign_key: true

      t.timestamps
    end
  end
end
