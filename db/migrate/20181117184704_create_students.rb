class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.string :cpf
      t.string :emails
      t.integer :enem_score

      t.timestamps
    end
  end
end
