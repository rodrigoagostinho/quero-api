class AddQuantityToEnrollment < ActiveRecord::Migration[5.2]
  def change
    add_column :enrollments, :quantity, :integer
  end
end
