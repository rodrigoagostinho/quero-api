class AddDocumentToStudent < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :document, :string
  end
end
